import axios from 'axios'
import router from '@/router'

const instance = axios.create({
  baseURL: 'http://localhost:3000'
})

function delay(sec: number) {
  return new Promise((resolve, reject) => {
    setTimeout(() => resolve(sec), sec * 1000)
  })
}
instance.interceptors.request.use(
  async function (config) {
    console.log('request interceptors')
    const token = localStorage.getItem('access_token')
    if (token) {
      config.headers.Authorization = 'Bearer ' + token
    }
    return config
  },
  function (error) {
    return Promise.reject(error)
  }
)
instance.interceptors.response.use(
  async function (res) {
    console.log('response interceptors')
    await delay(1)
    return res
  },
  function (error) {

    console.log(router)
    console.log('401')
    if (401 === error.response.status) {
      try {
        router.push({ path: '/login' })
      } catch (e) {
        console.log(e)
      }

    }
    return Promise.reject(error)
  }
)
export default instance
